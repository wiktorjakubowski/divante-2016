var DIVANTE = {};

jQuery(document).ready(function ($) {

    $.extend(window.DIVANTE, {

        init: function () {

            var taskId = $('body').attr('class');

            DIVANTE[taskId]();

        },

        zad1: function () {

            var unclickable = function () {

                var checkbox = $('input:checkbox');

                $(this).toggleClass('off');

                if ($(this).hasClass('off')) {
                    checkbox
                        .prop('disabled', true)
                        .prop('checked', false);
                } else {
                    checkbox
                        .prop('disabled', false);
                }
            };

            $(document).on('click', '.btn', unclickable);
        },

        zad2: function () {

            var elements = $('li'),
                biggerThenTen = function () {
                    return Number($(this).text()) > 10;
                };

            elements
                .filter(biggerThenTen)
                .css('color', 'red');
        },

        zad3: function () {

            var input = $('input:text'),
                isNumeric = function (n) {
                    return !isNaN(parseFloat(n)) && isFinite(n);
                },
                validateNumber = function () {
                    var val = $(this).val();

                    if (isNumeric(val)) {
                        $(this).css('border-color', 'green');
                    } else {
                        $(this).css('border-color', 'red');
                    }

                };

            input.each(validateNumber);
            input.blur(validateNumber);

        },

        zad4: function () {

            var row = $('tr'),
                result = 0,
                doMath = function (n1, n2, o) {
                    var num1 = parseFloat(n1),
                        num2 = parseFloat(n2);

                    if (o === '+') {
                        result = num1 + num2;
                    } else if (o === '-') {
                        result = num1 - num2;
                    } else if (o === '*') {
                        result = num1 * num2;
                    } else {
                        throw new Error('You passed wrong operator');
                    }

                    return result;
                },
                trimSelectorValue = function (context, selector) {
                    return $(context).find(selector).text().trim();
                };

            row.each(function () {

                var num1 = trimSelectorValue(this, '.num1'),
                    num2 = trimSelectorValue(this, '.num2'),
                    operator = trimSelectorValue(this, '.operator'),
                    sum = $(this).find('.sum');

                sum.text(doMath(num1, num2, operator));
            });

        },

        zad5: function () {

            $(document).on('click', '.btn', function () {

                var check = confirm('What is your choice?');

                if (check) {
                    alert('SUKCES');
                }
            });

        },

        zad6: function () {

            var formatNumber = function (x) {
                return String(x).replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            };

            $(document).on('click', '.btn', function () {

                var p = $('.well p'),
                    str = p.text(),
                    newStr = formatNumber(str);

                p.text(newStr);
            });

        },

        zad7: function () {

            var slider = $('.slider'),
                slides = slider.find('li'),
                index = 0,
                slidesNumber = slides.length,
                width = 1130,
                height = 200,
                pagination = $('.pagination');

            slider
                .wrapInner('<div class="slides-wrapper"></div>')
                .width(width)
                .height(height);

            slides.width(width);

            slides.each(function (i) {
                pagination.append('<button class="btn" data-slide=' + i + '>' + i + '</button>');
            });

            $(document).on('click', '.pagination button', function () {
                var slideNr = $(this).attr('data-slide');

                index = -slideNr;

                $('.slides-wrapper').css('left', width * index);
            });

            $(document).on('click', '.actions .btn', function (e) {

                var target = $(e.target);

                if (target.is('.pull-left') && index < 0) {
                    index++;
                    $('.slides-wrapper').css('left', width * index);

                } else if (target.is('.pull-right') && index > (-slidesNumber + 1)) {
                    index--;
                    $('.slides-wrapper').css('left', width * index);
                }

                return false;
            });
        },

        zad8: function () {

            var header = $('header.header'),
                offset = header.offset().top,
                headerHeight = header.outerHeight(),
                headerMarginBottom = header.css('margin-bottom');

            header.wrap('<div class="menu-wrapper"></div>');

            var scrollDebounced = _.debounce(function () {
                var scroll = $(document).scrollTop(),
                    newWidth = $('.well').width();

                if (offset - scroll <= 0) {
                    header.outerWidth(newWidth);
                    header.css({'position': 'fixed', 'top': '0'});
                    $('.menu-wrapper').height(headerHeight).css('margin-bottom', headerMarginBottom);
                } else {
                    header.css('position', 'relative');
                }
            });

            var resizeDebounced = _.debounce(function () {
                var newWidth = $('.well').width();
                header.outerWidth(newWidth);
                $('.menu-wrapper').height(headerHeight).css('margin-bottom', headerMarginBottom);
            });

            $(window).scroll(scrollDebounced);
            $(window).resize(resizeDebounced);

        },

        zad9: function () {

            var sidebar = $('.sidebar'),
                lists = sidebar.find('.list-group'),
                listsQty = lists.length;

            lists.innerHeight(100 / listsQty + '%');
            lists.mCustomScrollbar({
                theme: "minimal"
            });
        },

        zad10: function () {

            var module = $('.module'),
                rightContainer = $('.container__right');

            module.detach();
            module.addClass('module__right');
            rightContainer.append(module);

        }

    });

    DIVANTE.init();

});